"""Print `facebook.txt` cleaned."""


def clean(lines: list) -> list:
    """Return `lines` with only facebook links and no double."""
    cleaned = []

    for line in lines:
        if "facebook" in line.strip() and line not in cleaned:
            cleaned += [line]

    return cleaned


def run():
    """Print `facebook.txt` cleaned."""
    with open("facebook.txt", "rb") as file:
        b_file = file.read()

    s_file = str(b_file, encoding="utf-8")
    lines = s_file.split("\n")
    cleaned = clean(lines)

    for line in cleaned:
        print(line)


if __name__ == "__main__":
    run()
