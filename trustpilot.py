"""Filter trustpilot websites."""
import sys

from os import environ
from os.path import isfile

from time import sleep

from typing import Union

from subprocess import getstatusoutput

from bs4 import BeautifulSoup

from tqdm import tqdm


def get_score(html: str) -> float:
    """Return score from opinion `html`."""
    results = html.find_all("span", attrs={"class": "styles_desktop__3N0-b"})

    if len(results) != 1:
        return 6  # ignore

    result = results[0]
    text = result.text
    words = text.split(" ")

    if len(words) != 2:
        return 6  # ignore

    o_score = words[1]
    s_score = o_score.replace(",", ".")
    score = float(s_score)

    return score


def get_link(html: str) -> str:
    """Return link from opinion `html`."""
    links = html.find_all("a", attrs={
        "name": "business-unit-card",
        "target": "_self",
        "class": "link_internal__YpiJI link_wrapper__LEdx5 styles_linkWrapper__3x9X7",
        "data-business-unit-card-link": "true"
    })

    assert len(links) == 1

    link_html = links[0]

    return "https://fr.trustpilot.com" + link_html.attrs["href"]


def get_number_only(url: str) -> str:
    """Bypass weird encoding by returning only numbers."""
    numbers = ("0", "1", "2", "3", "4", "6", "7", "8", "9")

    ret = ""

    for char in url:
        if char in numbers:
            ret += char

    return ret


def get_not_digit(string: str) -> int:
    """Return an integer based upon `s`."""
    if string[0].isdigit() and string[-1].isdigit():
        # probably a thousand
        return int(get_number_only(string))

    return 0


def get_nb_opinions(html: str) -> int:
    """Return number of opinion from `html`. Read README."""
    class_ = "typography_typography__23IQz typography_bodysmall__24hZa typography_color-gray-7__2eGCj typography_weight-regular__iZYoT typography_fontstyle-normal__1_HQI"  # noqa
    results = html.find_all("p", attrs={"class": class_})

    if len(results) != 1:
        return 0

    pretty = results[0].prettify()

    lines = pretty.split("\n")

    line = lines[-4]

    s_nb = line.strip()

    if not s_nb.isdigit():
        return get_not_digit(s_nb)

    return int(s_nb)


def get_links(html: str) -> Union[list, str]:
    """Return."""
    links = []

    soup = BeautifulSoup(html, "html.parser")

    assert soup.body is not None, f"html: {html}"

    class_ = "paper_paper__29o4A card_card__2F_07 card_noPadding__1tkWv styles_wrapper__2QC-c styles_businessUnitCard__1-z5m"  # noqa
    opinions = soup.body.find_all("div", attrs={"class": class_})

    for opinion in opinions:
        score = get_score(opinion)
        nb_opinions = get_nb_opinions(opinion)

        if score >= 4:
            continue  # will return "score" if empty

        if nb_opinions < 50:
            return "opinion"

        link = get_link(opinion)
        if link not in links:
            links += [link]

    if not links:
        return "score"

    return links


def get_clean_url(url: str) -> str:
    """Return `url` but with only letters and numbers."""
    clean = ""

    if url[:8] == "https://":
        url = url[8:]

    allowed = (
        "a", "b", "c", "d", "e", "f", "g", "h", "i",
        "j", "k", "l", "m", "n", "o", "p", "q", "r",
        "s", "t", "u", "v", "w", "x", "y", "z", "0",
        "1", "2", "3", "4", "5", "6", "7", "8", "9"
    )

    for char in url:
        if char.lower() in allowed:
            clean += char

    return clean


def fetch_website_content(url: str) -> str:
    """Return website of `url` content from `curl`."""
    status, output = getstatusoutput("curl -s \"%s\"" % url)

    if status == 127:
        print("error: curl not found, this software does not work on Windows", file=sys.stderr)
        print(output, file=sys.stderr)
        sys.exit(1)

    if status != 0:
        print("error: failed to run curl", file=sys.stderr)
        print(output, file=sys.stderr)
        sys.exit(1)

    msg = "We have received an unusually large amount of requests from your IP so you have been rate limited"  # noqa
    if msg in output:
        print("error: too much requests", file=sys.stderr)
        sys.exit(2)

    return output


def get_website_content(url: str) -> str:
    """Return website of url `url` content from cache or `fetch_website_content()`."""
    clean_url = get_clean_url(url)
    cache_path = environ["HOME"] + "/.cache/trustpilot/" + clean_url

    if isfile(cache_path):
        with open(cache_path, "rb") as file:
            b_file = file.read()
        return str(b_file, encoding="utf-8")

    content = fetch_website_content(url)

    b_file = bytes(content, encoding="utf-8")
    with open(cache_path, "wb") as file:
        file.write(b_file)

    return content


def get_url_links(url: str) -> Union[list, str]:
    """Return links from `url`."""
    s_file = get_website_content(url)

    return get_links(s_file)


def get_category(cat: str) -> str:
    """Return category according to `sys.argv`."""
    category = "https://fr.trustpilot.com/categories/%s?numberofreviews=500&page={}&status=all&timeperiod=0"  # noqa

    return category % cat


def get_category_links(cat: str) -> list:
    """Return category links of the category `cat`."""
    category = get_category(cat)

    page = 2  # 1 is buggy, it redirects

    ret = []

    limit = get_pages(cat)

    bar = tqdm(total=limit, desc=cat)  # pylint: disable=C0104

    bar.update(2)

    while page < limit + 3:
        links = get_url_links(category.format(page))

        if links == "opinion":  # usually late
            break

        if isinstance(links, list):
            ret += links

        page += 1  # links "score" ++

        if page > limit + 2:
            break

        bar.update(1)

    bar.close()

    return ret


def get_websites(links: list) -> list:
    """Return Facebook."""
    get_website = lambda x: "https://" + x[33:]  # noqa
    # https://fr.trustpilot.com/review/ then website

    # return [get_website(link) for link in links]

    websites = []

    for link in links:
        website = get_website(link)
        if website not in websites:
            websites += [website]

    return websites


def get_facebook(website: str) -> str:
    """Return facebook if found or empty str if not."""
    command = r'curl -s "%s" | grep -Eo "facebook\.com\/\w{1,}"' % website
    status, output = getstatusoutput(command)

    if status != 0:
        return website  # not found

    return output


def run_category(category: str):
    """Print every facebook of `category`."""
    links = get_category_links(category)
    websites = get_websites(links)

    for website in websites:
        facebook = get_facebook(website) + "\n"
        b_facebook = bytes(facebook, encoding="utf-8")
        with open("facebook.txt", "ab") as file:
            file.write(b_facebook)


def get_pages(cat: str) -> int:
    """Return number of pages of category `cat`."""
    category = get_category(cat).format(2)

    content = get_website_content(category)  # cache, no unecessary

    soup = BeautifulSoup(content, "html.parser")

    results = soup.body.find_all("a", attrs={"name": "pagination-button-last"})

    if not results:  # very small page
        return 0

    assert len(results) == 1

    result = results[0]

    return int(result.text)


def run_categories():
    """Run `run_category` on every line of `categories.txt`."""
    with open("categories.txt", "rb") as file:
        b_file = file.read()
    s_file = str(b_file, encoding="utf-8")

    lines = s_file.split("\n")
    clean = [line.strip() for line in lines if line.strip()]

    for category in clean:
        if get_pages(category) != 0:
            try:
                run_category(category)
            except SystemExit as error:
                if str(error) == "2":  # too much request
                    sleep(1200)  # 20 minutes


def run():
    """Run."""
    if len(sys.argv) == 1:
        print("error: no input category", file=sys.stderr)
        sys.exit(1)

    if sys.argv[1] == ".":
        run_categories()
        return

    run_category(sys.argv[1])


if __name__ == "__main__":
    run()
